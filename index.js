const java = require('java')
const { DateTime } = require('luxon')

const JACKSON_VERSION = "2.9.8"


/* generate dependencies with
   mvn dependency:copy-dependencies
   */
const utils = "rgw-toolbox-4.2.7.jar"
const ann = `jackson-annotations-${JACKSON_VERSION}.jar`
const jackson = `jackson-core-${JACKSON_VERSION}.jar`
const databind = `jackson-databind-${JACKSON_VERSION}.jar`

const libdir = "./lib"

let path = require('path');
let fs = require('fs');
java.classpath.push(path.join(__dirname, libdir, utils));
java.classpath.push(path.join(__dirname, libdir, jackson));
java.classpath.push(path.join(__dirname, libdir, databind));
java.classpath.push(path.join(__dirname, libdir, ann));



if (!fs.existsSync(path.join(__dirname, libdir, utils))) {
  throw new Error(`${libdir}/${utils} not found!`)
}

/**
* Convert an Elexis ExtInfo field to json.
* @param {} buffer  the binary data as stored in the database
* @returns a JSON object with the contents of the ExtInfo (which might be {}
* if the input was empty or could not be read.)
*/
function decodeExtInfo(buffer) {
  if (buffer) {
    try {
      let array = java.newArray("byte",
        Array.prototype.slice.call(buffer, 0)
      )

      let extInfo = java.callStaticMethodSync("ch.rgw.tools.ExtInfo", "fold", array)
      if (extInfo) {
        let jsonstring = java.callStaticMethodSync("ch.rgw.tools.ExtInfo", "toJson", extInfo)
        return JSON.parse(jsonstring)
      }
    } catch (ex) {
      logger.error("getExtInfo:" + ex)
    }
  }
  return {}
}
/**
   * convert a JSON Object to ExtInfo
   * @param {} obj  some json
   * @returns a binary containing the ExtInfo ready to write into the database
   */
function encodeExtInfo(obj) {
  if (obj && obj != {}) {
    const str = JSON.stringify(obj)
    return java.callStaticMethodSync("ch.rgw.tools.ExtInfo", "flattenFromJson", str)
  } else {
    return null;
  }
}

/**
 * create an empty versionedResource
 */
function createVersionedResource() {
  let vr = java.callStaticMethodSync("ch.rgw.tools.VersionedResource", "load", null)
  let bindata = java.callMethodSync(vr, "serialize")
  return bindata
}


/**
   * Get the head revision of a VersionedResource
   * @param bindata: The binary data containing the versionedResource
   * @returns string: The head revision as IVersionedResource
   */
function getVersionedResource(bindata) {
  if (bindata && bindata.length) {
    let array = java.newArray("byte",
      Array.prototype.slice.call(bindata, 0)
    )
    let vr = java.callStaticMethodSync("ch.rgw.tools.VersionedResource", "load", array)
    let entry = java.callMethodSync(vr, "getHead")
    let lastVersion = java.callMethodSync(vr, "getHeadVersion")
    let item = java.callMethodSync(vr, "getVersion", lastVersion)
    let label = java.callMethodSync(item, "getLabel")
    var sl = label.split(/\s*-\s*/)

    return {
      text: entry,
      remark: sl[1],
      timestamp: DateTime.fromFormat(sl[0], "dd.LL.YYYY, HH:mm:ss").toISO,
      version: lastVersion
    }
  } else {
    return {
      text: "?",
      remark: "?",
      timestamp: DateTime.local().toISO(),
      version: 0
    }
  }
}

/**
   * Add a new version to a VersionedResource
   * @param entry the existing VersionedResource (binary)
   * @param newText the text to add
   * @param remark a remark for the new version
   * @returns {*} the updated VersionedResource
   */
function updateVersionedResource(entry, newText, remark) {
  let array = java.newArray("byte",
    Array.prototype.slice.call(entry, 0)
  )
  let vr = java.callStaticMethodSync("ch.rgw.tools.VersionedResource", "load", array)
  java.callMethodSync(vr, "update", newText, remark)
  let binfield = java.callMethodSync(vr, "serialize")
  return binfield
}

/**
 * Convert a JavaScript date to an array with varois types needed for elexis
 * @param {Date} date 
 */
function dateToElexisDateTime(date) {
  const ld = DateTime.fromJSDate(date)
  const ret = {
    day: ld.toFormat("yyyyLLdd"),
    hhmm: ld.toFormat("HHmm"),
    hhmmss: ld.toFormat("HHmmss"),
    minutes: ld.hour * 60 + ld.minute
  }
  return ret
}

/**
 * Convert an Elexis Date or DateTime to a javascript date
 * @param {YYYYMMDD | YYYYMMDDHHmm | YYYYMMDDHHmmss | {day,minutes}} elxdate 
 */
function elexisDateTimeToDate(elxdate) {
  let ret
  if (typeof (elxdate) == 'string') {
    if (elxdate.length == 8) {
      ret = DateTime.fromFormat(elxdate, "yyyyLLdd")
    } else if (elxdate.length == 12) {
      ret = DateTime.fromFormat(elxdate, "yyyyLLddHHmm")
    } else if (elxdate.length == 14) {
      ret = DateTime.fromFormat(elxdate, "yyyyLLddHHmmss")
    }
  } else {
    ret = DateTime.fromFormat(elxdate.day, "yyyyLLdd")
    ret = ret.plus({ minute: elxdate.minutes })
  }
  return ret.toJSDate()
}

module.exports = {
  decodeExtInfo,
  encodeExtInfo,
  dateToElexisDateTime,
  elexisDateTimeToDate,
  createVersionedResource,
  updateVersionedResource,
  getVersionedResource
}
