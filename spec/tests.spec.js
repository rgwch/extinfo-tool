const { decodeExtInfo,
  encodeExtInfo,
  elexisDateTimeToDate,
  dateToElexisDateTime,
  createVersionedResource,
  updateVersionedResource,
  getVersionedResource } = require('../index')

describe("elexis-types", () => {
  it("creates and decodes an extinfo", () => {
    const orig = {
      str: "A string",
      num: 5,
      bool: true,
      obj: {
        "one": 1, "two": "zwo", "three": false
      },
      arr: [1, 2, 3]
    }
    const encoded = encodeExtInfo(orig)
    expect(encoded).toBeDefined
    const decoded = decodeExtInfo(encoded)
    expect(decoded).toEqual(orig)
  })

  it("converts between elexis date and JSDate", () => {
    const el1 = "20190125"
    const d1 = elexisDateTimeToDate(el1)
    const el2 = { day: el1, minutes: 545 }
    const d2 = elexisDateTimeToDate(el2)
    const el3 = "201901250920"
    const d3 = elexisDateTimeToDate(el3)
    const el4 = "20190125092030"
    const d4 = elexisDateTimeToDate(el4)
    const r1=dateToElexisDateTime(d1)
    expect(r1.day).toEqual(el1)
    expect(r1.hhmm).toEqual("0000")
    expect(r1.hhmmss).toEqual("000000")
    expect(r1.minutes).toEqual(0)
    const r2=dateToElexisDateTime(d2)
    expect(r2.day).toEqual(el1)
    expect(r2.hhmm).toEqual("0905")
    expect(r2.hhmmss).toEqual("090500")
    expect(r2.minutes).toEqual(545)
    const r3=dateToElexisDateTime(d3)
    expect(r3.day).toEqual(el1)
    expect(r3.hhmm).toEqual("0920")
    expect(r3.hhmmss).toEqual("092000")
    expect(r3.minutes).toEqual(560)
    const r4=dateToElexisDateTime(d4)
    expect(r4.day).toEqual(el1)
    expect(r4.hhmm).toEqual("0920")
    expect(r4.hhmmss).toEqual("092030")
    expect(r4.minutes).toEqual(560)
  })

  it("creates, updates and reads VersionedResources", () => {
    const vr = createVersionedResource()
    const vr1 = updateVersionedResource(vr, "That's text", "remark")
    const head = getVersionedResource(vr1)
    expect(head.text).toEqual("That's text")
    expect(head.remark).toEqual("remark")
    const vr2 = updateVersionedResource(vr1, "A second version", "other remark")
    const newhead = getVersionedResource(vr2)
    expect(newhead.text).toEqual("A second version")
    expect(newhead.remark).toEqual("other remark")
  })
})